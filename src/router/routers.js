import Main from '@/components/main'

export default [
  {
    path: '/login',
    name: 'login',
    meta: {
      title: 'Login - 登录',
      hideInMenu: true
    },
    component: () => import('@/view/login/login.vue')
  },
  {
    path: '/',
    name: '_home',
    redirect: '/home',
    component: Main,
    meta: {
      hideInMenu: true,
      notCache: true
    },
    children: [
      {
        path: '/home',
        name: 'home',
        meta: {
          hideInMenu: true,
          title: '首页',
          notCache: true,
          icon: 'md-home'
        },
        component: () => import('@/view/single-page/home')
      }
    ]
  },
  {
    path: '/sales',
    name: 'sales',
    redirect: '/sale_list',
    meta: {
      icon: 'logo-buffer',
      title: '销售管理',
      showAlways: true
    },
    component: Main,
    children: [
      {
        path: '/sale_list',
        name: 'sale_list',
        meta: {
          icon: 'md-trending-up',
          title: '今日报表'
        },
        component: () => import('@/view/sales/sale-list.vue')
      }, {
        path: '/sale_statistics',
        name: 'sale_statistics',
        meta: {
          icon: 'md-trending-up',
          title: '销售统计'
        },
        component: () => import('@/view/sales/statistics-list.vue')
      },
      {
        path: '/salestarget',
        name: 'salestarget',
        meta: {
          icon: 'md-trending-up',
          title: '销售目标'
        },
        component: () => import('@/view/sales/sales-target.vue')
      }, {
        path: '/ranking',
        name: 'ranking',
        meta: {
          icon: 'md-trending-up',
          title: '员工排行'
        },
        component: () => import('@/view/sales/ranking.vue')
      }
    ]
  },
  {
    path: '/customer',
    name: 'customer',
    redirect: '/customer_list',
    meta: {
      icon: 'logo-buffer',
      title: '客服管理',
      showAlways: true
    },
    component: Main,
    children: [
      {
        path: '/customer_list',
        name: 'customer_list',
        meta: {
          icon: 'md-trending-up',
          title: '今日报表'
        },
        component: () => import('@/view/customer/customer_list.vue')
      }, {
        path: '/statistical',
        name: 'statistical',
        meta: {
          icon: 'md-trending-up',
          title: '统计报表'
        },
        component: () => import('@/view/customer/statistical_list.vue')
      }, {
        path: '/operating',
        name: 'operating',
        meta: {
          icon: 'md-trending-up',
          title: '运营项目'
        },
        component: () => import('@/view/customer/operating_list.vue')
      }, {
        path: '/percost',
        name: 'percost',
        meta: {
          icon: 'md-trending-up',
          title: '每日成本'
        },
        component: () => import('@/view/customer/percost_list.vue')
      }
    ]
  },
  {
    path: '/business',
    name: 'business',
    redirect: '/business_list',
    meta: {
      icon: 'logo-buffer',
      title: '商务管理',
      showAlways: true
    },
    component: Main,
    children: [
      {
        path: '/business_list',
        name: 'business_list',
        meta: {
          icon: 'md-trending-up',
          title: '今日报表'
        },
        component: () => import('@/view/business/business-list.vue')
      }
    ]
  },
  {
    path: '/personnel',
    name: 'personnel',
    redirect: 'personnel_list',
    meta: {
      icon: 'logo-buffer',
      title: '人事管理',
      showAlways: true
    },
    component: Main,
    children: [
      {
        path: '/personnel_list',
        name: 'personnel_list',
        meta: {
          icon: 'md-trending-up',
          title: '今日报表'
        },
        component: () => import('@/view/personnel/personnel-list.vue')
      }, {
        path: '/employee',
        name: 'employee',
        meta: {
          icon: 'md-trending-up',
          title: '职员信息'
        },
        component: () => import('@/view/personnel/employee-list.vue')
      }, {
        path: '/personnel_statistics',
        name: 'personnel_statistics',
        meta: {
          icon: 'md-trending-up',
          title: '人事统计'
        },
        component: () => import('@/view/personnel/statistics-list.vue')
      }, {
        path: '/departure',
        name: 'departure',
        meta: {
          icon: 'md-trending-up',
          title: '离职统计'
        },
        component: () => import('@/view/personnel/departure-list.vue')
      }, {
        path: '/department',
        name: 'department',
        meta: {
          icon: 'md-trending-up',
          title: '部门信息'
        },
        component: () => import('@/view/personnel/department-info.vue')
      }
    ]
  },
  {
    path: '/system',
    name: 'system',
    redirect: '/user_list',
    meta: {
      icon: 'logo-buffer',
      title: '系统管理',
      showAlways: true
    },
    component: Main,
    children: [
      // {
      //   path: 'user_manager',
      //   name: 'user_manager',
      //   meta: {
      //     icon: 'md-trending-up',
      //     title: '用户管理'
      //   },
      //   component: () => import('@/view/system/user-manager.vue')
      // },
      // {
      //   path: 'sys_config',
      //   name: 'sys_config',
      //   meta: {
      //     icon: 'md-trending-up',
      //     title: '系统配置'
      //   },
      //   component: () => import('@/view/system/sys-config.vue')
      // },
      {
        path: '/user_list',
        name: 'user_list',
        meta: {
          icon: 'md-trending-up',
          title: '用户列表'
        },
        component: () => import('@/view/system/user_list.vue')
      }
    ]
  },
  {
    path: '/401',
    name: 'error_401',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/view/error-page/401.vue')
  },
  {
    path: '/500',
    name: 'error_500',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/view/error-page/500.vue')
  },
  {
    path: '*',
    name: 'error_404',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/view/error-page/404.vue')
  }
]
