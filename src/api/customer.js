import axios from '@/libs/api.request'
import config from '@/config'

export const getTableData = async (playload) => {
  const resp = await axios.request({
    url: config.apiUrl,
    params: {
      module: 'OrderService.list',
      ...playload
    },
    method: 'get'
  })
  return resp
}

export const getStatisticalTable = async (playload) => {
  const resp = await axios.request({
    url: config.apiUrl,
    params: {
      module: 'OrderService.recordList',
      ...playload
    },
    method: 'get'
  })
  return resp
}

export const getOperateTable = async (playload) => {
  const resp = await axios.request({
    url: config.apiUrl,
    params: {
      module: 'OrderService.projects',
      ...playload
    },
    method: 'get'
  })
  return resp
}

export const getPerCost = async (playload) => {
  const resp = await axios.request({
    url: config.apiUrl,
    params: {
      module: 'OrderService.percost',
      ...playload
    },
    method: 'get'
  })
  return resp
}

export const editCustomerRecord = async (playload) => {
  const data = {
    module: 'OrderService.add',
    ...playload
  }
  const resp = await axios.request({
    url: config.postUrl,
    data,
    method: 'post'
  })
  return resp
}

export const editOperateRecord = async (playload) => {
  const data = {
    module: 'OrderService.addProject',
    ...playload
  }
  const resp = await axios.request({
    url: config.postUrl,
    data,
    method: 'post'
  })
  return resp
}

export const editStatisticalRecord = async (playload) => {
  const data = {
    module: 'OrderService.saverecord',
    ...playload
  }
  const resp = await axios.request({
    url: config.postUrl,
    data,
    method: 'post'
  })
  return resp
}

export const savePerCost = async (playload) => {
  const data = {
    module: 'OrderService.savePercost',
    ...playload
  }
  const resp = await axios.request({
    url: config.postUrl,
    data,
    method: 'post'
  })
  return resp
}
