import axios from '@/libs/api.request'
import config from '@/config'

export const getDepartmentTable = async (playload) => {
  const resp = await axios.request({
    url: config.apiUrl,
    params: {
      module: 'DepartmentService.view',
      ...playload
    },
    method: 'get'
  })
  return resp
}

export const editeDepartmentRecord = async (playload) => {
  let module_name = 'DepartmentService.add'
  if (playload.id && playload.id !== '') {
    module_name = 'DepartmentService.update'
  }
  const data = {
    module: module_name,
    ...playload

  }
  const resp = await axios.request({
    url: config.postUrl,
    data,
    method: 'post'
  })
  return resp
}
