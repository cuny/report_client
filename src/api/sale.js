import axios from '@/libs/api.request'
import config from '@/config'

export const getSaleTable = async (playload) => {
  const resp = await axios.request({
    url: config.apiUrl,
    params: {
      module: 'SellService.view',
      ...playload
    },
    method: 'get'
  })
  return resp
}

export const getSaleRecord = async (playload) => {
  const resp = await axios.request({
    url: config.apiUrl,
    params: {
      module: 'SellService.recordList',
      ...playload
    },
    method: 'get'
  })
  return resp
}

export const getSaleTarget = async (playload) => {
  const resp = await axios.request({
    url: config.apiUrl,
    params: {
      module: 'SellService.salestarget',
      ...playload
    },
    method: 'get'
  })
  return resp
}

export const getRanking = async (playload) => {
  const resp = await axios.request({
    url: config.apiUrl,
    params: {
      module: 'SellService.ranking',
      ...playload
    },
    method: 'get'
  })
  return resp
}

export const saveSaleRecord = async (playload) => {
  const resp = await axios.request({
    url: config.apiUrl,
    params: {
      module: 'SellService.saverecord',
      ...playload
    },
    method: 'get'
  })
  return resp
}

export const editeSaleRecord = async (playload) => {
  let module_name = 'SellService.friend'
  if (playload.id && playload.id !== '') {
    module_name = 'SellService.update'
  }
  const data = {
    module: module_name,
    ...playload
  }
  const resp = await axios.request({
    url: config.postUrl,
    data,
    method: 'post'
  })
  return resp
}

export const editeSaleTarget = async (playload) => {
  let module_name = 'SellService.addtarget'
  // if (playload.id && playload.id !== '') {
  //   module_name = 'SellService.update'
  // }
  const data = {
    module: module_name,
    ...playload
  }
  const resp = await axios.request({
    url: config.postUrl,
    data,
    method: 'post'
  })
  return resp
}

// export const addSaleRecord = async (playload) => {
//   const resp = await axios.request({
//     url: config.apiUrl,
//     params: {
//       module: 'SellService.friend',
//       ...playload
//     },
//     method: 'get'
//   })
//   return resp
// }

// export const updateSaleRecord = async (playload) => {
//   const resp = await axios.request({
//     url: config.apiUrl,
//     params: {
//       module: 'SellService.update',
//       ...playload
//     },
//     method: 'get'
//   })
//   return resp
// }
