import axios from '@/libs/api.request'
import config from '@/config'

export const getBusinessTable = async (playload) => {
  const resp = await axios.request({
    url: config.apiUrl,
    params: {
      module: 'TradeService.view',
      ...playload
    },
    method: 'get'
  })
  return resp
}

export const editBusinessRecord = async (playload) => {
  let module_name = 'TradeService.add'
  if (playload.id && playload.id !== '') {
    module_name = 'TradeService.update'
  }
  const data = {
    module: module_name,
    ...playload
  }
  const resp = await axios.request({
    url: config.postUrl,
    data,
    method: 'post'
  })
  return resp
}
