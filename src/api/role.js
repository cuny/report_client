import axios from '@/libs/api.request'
import config from '@/config'

export const getRole = async (playload) => {
  const resp = await axios.request({
    url: config.apiUrl,
    params: {
      module: 'RoleService.group',
      ...playload
    },
    method: 'get'
  })
  return resp
}
