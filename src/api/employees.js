import axios from '@/libs/api.request'
import config from '@/config'

export const getPersonnelTable = async (playload) => {
  const resp = await axios.request({
    url: config.apiUrl,
    params: {
      module: 'EmployeeService.view',
      ...playload
    },
    method: 'get'
  })
  return resp
}

export const getEmployeesTable = async (playload) => {
  const resp = await axios.request({
    url: config.apiUrl,
    params: {
      module: 'EmployeeService.employees',
      ...playload
    },
    method: 'get'
  })
  return resp
}

export const getReportTable = async (playload) => {
  const resp = await axios.request({
    url: config.apiUrl,
    params: {
      module: 'EmployeeService.report',
      ...playload
    },
    method: 'get'
  })
  return resp
}

export const getTurnOverTable = async (playload) => {
  const resp = await axios.request({
    url: config.apiUrl,
    params: {
      module: 'EmployeeService.turnOver',
      ...playload
    },
    method: 'get'
  })
  return resp
}

export const editePersonnelRecord = async (playload) => {
  let module_name = 'EmployeeService.add'
  if (playload.id && playload.id !== '') {
    module_name = 'EmployeeService.progress'
  }
  const data = {
    module: module_name,
    ...playload

  }
  const resp = await axios.request({
    url: config.postUrl,
    data,
    method: 'post'
  })
  return resp
}

export const editeEmployeeRecord = async (playload) => {
  let module_name = 'EmployeeService.addEmpoy'
  if (playload.id && playload.id !== '') {
    module_name = 'EmployeeService.updateEmploy'
  }
  const data = {
    module: module_name,
    ...playload
  }
  const resp = await axios.request({
    url: config.postUrl,
    data,
    method: 'post'
  })
  return resp
}

export const uploadFile = async (playload) => {
  const data = {
    // module: module_name,
    ...playload
  }
  const resp = await axios.request({
    url: config.uploadUrl,
    data,
    method: 'post'
  })
  return resp
}
