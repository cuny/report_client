import axios from '@/libs/api.request'
import config from '@/config'

export const login = async ({ userName, password }) => {
  const data = {
    tel: userName,
    password,
    module: 'UserService.loginBypwd'
  }
  const resp = await axios.request({
    url: config.postUrl,
    data,
    method: 'post'
  })
  return resp
}

export const getMenus = async () => {
  const resp = await axios.request({
    url: config.apiUrl,
    params: {
      module: 'RoleService.view'
    },
    method: 'get'
  })
  return resp
}

export const logout = (token) => {
  return axios.request({
    url: 'logout',
    method: 'post'
  })
}

export const getUnreadCount = () => {
  return axios.request({
    url: 'message/count',
    method: 'get'
  })
}

export const getMessage = () => {
  return axios.request({
    url: 'message/init',
    method: 'get'
  })
}

export const getContentByMsgId = msg_id => {
  return axios.request({
    url: 'message/content',
    method: 'get',
    params: {
      msg_id
    }
  })
}

export const hasRead = msg_id => {
  return axios.request({
    url: 'message/has_read',
    method: 'post',
    data: {
      msg_id
    }
  })
}

export const removeReaded = msg_id => {
  return axios.request({
    url: 'message/remove_readed',
    method: 'post',
    data: {
      msg_id
    }
  })
}

export const restoreTrash = msg_id => {
  return axios.request({
    url: 'message/restore',
    method: 'post',
    data: {
      msg_id
    }
  })
}
