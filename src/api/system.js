import axios from '@/libs/api.request'
import config from '@/config'

export const getUserListTable = async () => {
  const resp = await axios.request({
    url: config.apiUrl,
    params: {
      module: 'UserService.view'
    },
    method: 'get'
  })
  return resp
}

export const editUserlistRecord = async (playload) => {
  let module_name = 'UserService.add'
  if (playload.id && playload.id !== '') {
    module_name = 'UserService.update'
  }
  const data = {
    module: module_name,
    ...playload
  }
  const resp = await axios.request({
    url: config.postUrl,
    data,
    method: 'post'
  })
  return resp
}

export const getRole = async (playload) => {
  const resp = await axios.request({
    url: config.apiUrl,
    params: {
      module: 'RoleService.group',
      ...playload
    },
    method: 'get'
  })
  return resp
}

export const getPid = async (playload) => {
  const resp = await axios.request({
    url: config.apiUrl,
    params: {
      module: 'UserService.pids',
      ...playload
    },
    method: 'get'
  })
  return resp
}
