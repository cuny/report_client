import axios from '@/libs/api.request'
import config from '@/config'

export const getMainReportData = async (payload) => {
  const resp = await axios.request({
    url: config.apiUrl,
    params: {
      module: 'OrderService.proReport'
    },
    method: 'get'
  })
  return resp
}
