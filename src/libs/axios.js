import axios from 'axios'
import store from '@/store'
import qs from 'qs'
import { getToken } from './util'
// import { Spin } from 'iview'
import { Message } from 'iview'

class HttpRequest {
  constructor (baseUrl = baseURL) {
    this.baseUrl = baseUrl
    this.queue = {}
  }
  getInsideConfig () {
    const config = {
      baseURL: this.baseUrl,
      headers: {
        //
        'agent': 'Web'
      }
    }
    return config
  }
  destroy (url) {
    delete this.queue[url]
    if (!Object.keys(this.queue).length) {
      // Spin.hide()
    }
  }
  interceptors (instance, url) {
    // 请求拦截
    instance.interceptors.request.use(config => {
      // 添加全局的loading...
      // console.log(config.data instanceof Object)
      if (config.method === 'post') {
        config.headers['content-type'] = 'application/x-www-form-urlencoded'
        config.data = qs.stringify(config.data)
      }
      if (getToken()) {
        config.headers.token = getToken()
        this.queue[url] = true
      }
      return config
    }, error => {
      return Promise.reject(error)
    })
    // 响应拦截
    instance.interceptors.response.use(res => {
      this.destroy(url)
      const { data, status } = res
      return { data, status }
    }, error => {
      this.destroy(url)
      // console.log(error)
      // let errorInfo = error.response
      // if (!errorInfo) {
      //   const { request: { statusText, status }, config } = JSON.parse(JSON.stringify(error))
      //   errorInfo = {
      //     statusText,
      //     status,
      //     request: { responseURL: config.url }
      //   }
      // }
      // addErrorLog(errorInfo)
      return Promise.reject(error)
    })
  }
  request (options) {
    const instance = axios.create()
    options = Object.assign(this.getInsideConfig(), options)
    // console.log(options.data)
    this.interceptors(instance, options.url)
    return instance(options)
  }
}
export default HttpRequest
