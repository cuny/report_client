import { forEach } from '@/libs/tools'

export const getMenuList = (data) => {
  let menus = []
  let original = ['login', 'error_401', 'error_500', 'error_404']
  forEach(data, item => {
    menus.push(item.uri)
    if (item.children) {
      forEach(item.children, child => {
        menus.push(child.uri)
      })
    }
  })
  return menus.concat(original)
}
