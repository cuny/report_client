import {
  getRanking
} from '../../api/sale'
import {
  RANK_LIST,
  RANK_CURRENT_ROW,
  RANK_DATA_COUNT,
  RANK_HISTORY_DATA,
  RANK_UPLOAD_LIST
} from '../motations-type'

export default {
  state: {
    rankList: [],
    rankDataCount: null,
    pageSize: 20,
    rankHistoryData: [],
    row: {
      uname: '',
      cpa: '',
      cpv: '',
      ycpv: '',
      cps: '',
      cpsb: '',
      ctime: ''
    }
  },
  getters: {
    rankList: state => state.rankList
  },
  mutations: {
    [RANK_LIST] (state, data) {
      state.rankList = data
    },
    [RANK_CURRENT_ROW] (state, data) {
      state.row = data
    },
    [RANK_DATA_COUNT] (state, data) {
      state.rankDataCount = data
    },
    [RANK_HISTORY_DATA] (state, data) {
      state.rankHistoryData = data
    },
    [RANK_UPLOAD_LIST] (state, data) {
      state.rankList = state.rankList.push(data)
    }
  },
  actions: {
    async getRankList ({ commit, rootState }, playload) {
      let data = {}
      let newData = []
      if (Object.keys(playload).length > 0) {
        data = playload
      }
      const resp = await getRanking(data)
      resp.data.data.rows.map((item) => {
        newData = [...newData, {
          id: item.id,
          uname: item.uname ? item.uname : '-',
          cpa: item.cpa,
          cpv: item.cpv,
          ycpv: item.ycpv,
          cps: item.cps,
          cpsb: item.cpsb,
          ctime: item.ctime
        }]
      })
      commit(RANK_LIST, newData)
      commit(RANK_DATA_COUNT, resp.data.data.total)
      if (newData.length < rootState.rank.pageSize) {
        commit(RANK_HISTORY_DATA, newData)
      } else {
        commit(RANK_HISTORY_DATA, newData.slice(0, rootState.rank.pageSize))
      }
    },
    // async updateSaleRecordData ({ commit, rootState }, playload) {
    //   let data = {}
    //   if (Object.keys(playload).length > 0) {
    //     data = playload
    //   }
    //   await editeSaleRecord(data)
    // },
    async getRankCurrentRow ({ commit, rootState }, row) {
      commit(RANK_CURRENT_ROW, row)
    },
    async changeRankPage ({ commit, rootState }, index) {
      let data = rootState.rank.rankList
      let _start = (index - 1) * rootState.rank.pageSize
      let _end = index * rootState.rank.pageSize
      commit(RANK_HISTORY_DATA, data.slice(_start, _end))
    },
    async getRankUploadList ({ commit, rootState }, uploadList) {
      commit(RANK_UPLOAD_LIST, uploadList)
    }
  }
}
