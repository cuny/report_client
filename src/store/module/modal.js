import { MODAL_STATE } from '../motations-type'

export default {
  state: {
    modalState: false
  },
  getters: {
    modalState: state => state.modalState
  },
  mutations: {
    [MODAL_STATE] (state, data) {
      state.modalState = data
    }
  },
  actions: {
    async updateModalState ({ commit, rootState }, playload) {
      commit(MODAL_STATE, playload)
    }
  }
}
