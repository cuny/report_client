import {
  getEmployeesTable,
  editeEmployeeRecord
} from '../../api/employees'
import {
  EMPLOYEE_LIST,
  EMPLOYEE_CURRENT_ROW,
  EMPLOYEE_DATA_COUNT,
  EMPLOYEE_HISTORY_DATA,
  EMPLOYEE_UPLOAD_LIST
} from '../motations-type'

export default {
  state: {
    employeeList: [],
    employeeDataCount: null,
    pageSize: 20,
    employeeHistoryData: [],
    row: {
      name: '',
      tel: '',
      num: '',
      intime: '',
      leavetime: '',
      state: '',
      des: '',
      icon: '',
      deptid: '',
      muid: '',
      id: '',
      job: '',
      color: '',
      sex: '',
      contract: '',
      expiredday: '',
      contractstart: '',
      contractend: '',
      probation: '',
      probationmind: '',
      fullday: '',
      sitime: '',
      idcard: '',
      birthmonth: '',
      birthday: '',
      birthmind: '',
      age: '',
      coage: '',
      people: '',
      marriage: '',
      emergency: '',
      rel: '',
      etel: '',
      school: '',
      ab: '',
      abcommit: '',
      study: '',
      selfposition: '',
      native: '',
      residence: '',
      nativeres: '',
      householdtype: '',
      annualtime: '',
      bankcard: '',
      backcard: '',
      bankname: '',
      subbank: '',
      bankprovince: '',
      bankcity: '',
      ssn: '',
      publicfund: '',
      pid: '',
      rootdept: '',
      subdept: ''
    }
  },
  getters: {
    employeeList: state => state.employeeList
  },
  mutations: {
    [EMPLOYEE_LIST] (state, data) {
      state.employeeList = data
    },
    [EMPLOYEE_CURRENT_ROW] (state, data) {
      state.row = data
    },
    [EMPLOYEE_DATA_COUNT] (state, data) {
      state.employeeDataCount = data
    },
    [EMPLOYEE_HISTORY_DATA] (state, data) {
      state.employeeHistoryData = data
    },
    [EMPLOYEE_UPLOAD_LIST] (state, data) {
      state.employeeList = state.employeeList.push(data)
    }
  },
  actions: {
    async getEmployeesList ({ commit, rootState }, playload) {
      let data = {}
      let newData = []
      if (Object.keys(playload).length > 0) {
        data = playload
      }
      const resp = await getEmployeesTable(data)
      resp.data.data.rows.map((item) => {
        newData = [...newData, {
          id: item.id,
          name: item.name,
          tel: item.tel.toString(),
          num: item.num ? item.num : '-',
          intime: item.intime,
          leavetime: item.leavetime,
          state: item.state.toString(),
          des: item.des ? item.des : '-',
          icon: item.icon ? item.icon : '',
          deptid: item.deptid ? item.deptid.toString() : '',
          deptname: item.deptname ? item.deptname : '-',
          muid: item.muid ? item.muid : '-',
          job: item.job ? item.job : '-',
          color: item.color ? item.color : '-',
          sex: item.sex ? item.sex : '-',
          contract: item.contract ? item.contract : '-',
          expiredday: item.expiredday,
          contractstart: item.contractstart,
          contractend: item.contractend,
          probation: item.probation,
          probationmind: item.probationmind,
          fullday: item.fullday,
          sitime: item.sitime,
          idcard: item.idcard ? item.idcard : '-',
          birthmonth: item.birthmonth ? item.birthmonth.toString() : '',
          birthday: item.birthday,
          birthmind: item.birthmind,
          age: item.age ? item.age : '',
          coage: item.coage,
          people: item.people ? item.people : '-',
          marriage: item.marriage ? item.marriage : '-',
          emergency: item.emergency ? item.emergency : '-',
          rel: item.rel ? item.rel : '-',
          etel: item.etel ? item.etel : '',
          school: item.school ? item.school : '-',
          ab: item.ab ? item.ab : '-',
          abcommit: item.abcommit ? item.abcommit : '-',
          study: item.study ? item.study : '-',
          selfposition: item.selfposition ? item.selfposition : '-',
          native: item.native ? item.native : '-',
          residence: item.residence ? item.residence : '-',
          nativeres: item.nativeres ? item.nativeres : '-',
          householdtype: item.householdtype ? item.householdtype : '-',
          annualtime: item.annualtime,
          bankcard: item.bankcard ? item.bankcard : '-',
          backcard: item.backcard ? item.backcard : '-',
          bankname: item.bankname ? item.bankname : '-',
          subbank: item.subbank ? item.subbank : '-',
          bankprovince: item.bankprovince ? item.bankprovince : '-',
          bankcity: item.bankcity ? item.bankcity : '-',
          ssn: item.ssn ? item.ssn : '-',
          publicfund: item.publicfund ? item.publicfund : '-',
          pid: item.pid ? item.pid : '-',
          rootdept: item.rootdept ? item.rootdept : '-',
          subdept: item.subdept ? item.subdept : '-'
        }]
      })
      commit(EMPLOYEE_LIST, newData)
      commit(EMPLOYEE_DATA_COUNT, resp.data.data.total)
      if (newData.length < rootState.employee.pageSize) {
        commit(EMPLOYEE_HISTORY_DATA, newData)
      } else {
        commit(EMPLOYEE_HISTORY_DATA, newData.slice(0, rootState.employee.pageSize))
      }
    },
    async changeEmployeePage ({ commit, rootState }, index) {
      let data = rootState.employee.employeeList
      let _start = (index - 1) * rootState.employee.pageSize
      let _end = index * rootState.employee.pageSize
      commit(EMPLOYEE_HISTORY_DATA, data.slice(_start, _end))
    },
    async editeEmployeeData ({ commit, rootState }, playload) {
      let data = {}
      if (Object.keys(playload).length > 0) {
        data = playload
      }
      await editeEmployeeRecord(data)
    },
    async getEmployeeCurrentRow ({ commit, rootState }, row) {
      commit(EMPLOYEE_CURRENT_ROW, row)
    },
    async getEmployeeUploadList ({ commit, rootState }, uploadList) {
      commit(EMPLOYEE_UPLOAD_LIST, uploadList)
    }
  }
}
