// eslint-disable-next-line
import {
  getTableData,
  editCustomerRecord
} from '../../api/customer'
import {
  CUSTOMER_LIST,
  CUSTOMER_CURRENT_ROW,
  CUSTOMER_DATA_COUNT,
  CUSTOMER_HISTORY_DATA,
  CUSTOMER_UPLOAD_LIST
} from '../motations-type'

export default {
  state: {
    customerData: [],
    customerDataCount: null,
    pageSize: 20,
    customerHistoryData: [],
    row: {
      proname: '',
      name: '',
      tel: '',
      price: '',
      ordtime: '',
      repeat: '',
      pcode: '',
      uname: '',
      pname: '',
      type: '',
      mstate: '',
      des: ''
    }
  },
  getters: {
    customerData: state => state.customerData
  },
  mutations: {
    [CUSTOMER_LIST] (state, data) {
      state.customerData = data
    },
    [CUSTOMER_CURRENT_ROW] (state, data) {
      state.row = data
    },
    [CUSTOMER_DATA_COUNT] (state, data) {
      state.customerDataCount = data
    },
    [CUSTOMER_HISTORY_DATA] (state, data) {
      state.customerHistoryData = data
    },
    [CUSTOMER_UPLOAD_LIST] (state, data) {
      state.customerData = state.customerData.push(data)
    }
  },
  actions: {
    async getCustomerList ({ commit, rootState }, playload) {
      let data = {}
      let newData = []
      if (Object.keys(playload).length > 0) {
        data = playload
      }
      const resp = await getTableData(data)
      resp.data.data.rows.map((item) => {
        newData = [...newData, {
          id: item.id ? item.id : '-',
          proname: item.proname,
          name: item.name ? item.name : '-',
          tel: item.tel ? item.tel : '-',
          proid: item.proid,
          price: item.price,
          ctime: item.ctime,
          ordtime: item.ordtime,
          repeat: item.repeat.toString(),
          pcode: item.pcode ? item.pcode : '-',
          uname: item.uname ? item.uname : '-',
          pname: item.pname ? item.pname : '-',
          type: item.type,
          mstate: item.mstate,
          des: item.des ? item.des : '-',
          cz: item.cz
        }]
      })
      commit(CUSTOMER_LIST, newData)
      commit(CUSTOMER_DATA_COUNT, resp.data.data.total)
      if (newData.length < rootState.customer.pageSize) {
        commit(CUSTOMER_HISTORY_DATA, newData)
      } else {
        commit(CUSTOMER_HISTORY_DATA, newData.slice(0, rootState.customer.pageSize))
      }
    },
    async changeCustomerPage ({ commit, rootState }, index) {
      let data = rootState.customer.customerData
      let _start = (index - 1) * rootState.customer.pageSize
      let _end = index * rootState.customer.pageSize
      commit(CUSTOMER_HISTORY_DATA, data.slice(_start, _end))
    },
    async getCustomerCurrentRow ({ commit, rootState }, row) {
      commit(CUSTOMER_CURRENT_ROW, row)
    },
    async editCustomerData ({ commit, rootState }, playload) {
      let data = {}
      if (Object.keys(playload).length > 0) {
        data = playload
      }
      await editCustomerRecord(data)
    },
    async getCustomerUploadList ({ commit, rootState }, uploadList) {
      commit(CUSTOMER_UPLOAD_LIST, uploadList)
    }
  }
}
