import { REPORT_DATA, CARD_DATA } from '../motations-type'
import { getMainReportData } from '../../api/home'
import { dataArrayList } from '@/libs/util'

export default {
  state: {
    mainReportData: window.sessionStorage.getItem('report_data')
      ? JSON.parse(window.sessionStorage.getItem('report_data'))
      : {},
    mainCardData: window.sessionStorage.getItem('card_data')
      ? JSON.parse(window.sessionStorage.getItem('card_data'))
      : []
  },
  actions: {
    getMainReportData: async ({ commit }) => {
      window.sessionStorage.removeItem('report_data')
      window.sessionStorage.removeItem('card_data')
      await getMainReportData().then(res => {
        dataArrayList(res.data).then(resp => {
          const card_data = [
            {
              title: '今日预约到访商户人数',
              icon: 'md-person-add',
              count: resp.vistreport.todaycnt,
              color: '#2d8cf0',
              symbol: '人'
            },
            {
              title: '明日预约到访商户人数',
              icon: 'md-locate',
              count: resp.vistreport.tomorrowcnt,
              color: '#19be6b',
              symbol: '人'
            },
            {
              title: '今日预约到访面试人数',
              icon: 'md-help-circle',
              count: resp.viewreport.todayviewcnt,
              color: '#ff9900',
              symbol: '人'
            },
            {
              title: '明日预约到访面试人数',
              icon: 'md-share',
              count: resp.viewreport.tomorrowviewcnt,
              color: '#ed3f14',
              symbol: '人'
            },
            {
              title: '月初在职人数',
              icon: 'md-chatbubbles',
              count: resp.employreport.cnum,
              color: '#19be6b',
              symbol: '人'
            },
            {
              title: '月入职人数',
              icon: 'md-map',
              count: resp.employreport.leavenum,
              color: '#9A66E4',
              symbol: '人'
            },
            {
              title: '月离职人数',
              icon: 'md-trending-down',
              count: resp.employreport.innum,
              color: '#36cfc9',
              symbol: '人'
            },
            {
              title: '月净增长率',
              icon: 'md-trending-up',
              count: parseFloat(resp.employreport.rate) * 100,
              color: '#ffa39e',
              symbol: '%'
            }
          ]
          commit(REPORT_DATA, resp)
          commit(CARD_DATA, card_data)
        })
      })
    }
  },
  getters: {
    mainReportData: state => state.mainReportData,
    mainCardData: state => state.mainCardData
  },
  mutations: {
    [REPORT_DATA] (state, data) {
      state.mainReportData = data
      window.sessionStorage.setItem('report_data', JSON.stringify(data))
    },
    [CARD_DATA] (state, data) {
      state.mainCardData = data
      window.sessionStorage.setItem('card_data', JSON.stringify(data))
    }
  }
}
