import {
  getUserListTable,
  editUserlistRecord,
  getRole,
  getPid
} from '../../api/system'
import {
  SYSTEM_USER_LIST,
  USERLIST_CURRENT_ROW,
  USERLIST_DATA_COUNT,
  USERLIST_HISTORY_DATA,
  ROLE_LIST,
  PID_LIST,
  SYSTEM_USER_UPLOAD_LIST
} from '../motations-type'

export default {
  state: {
    userList: [],
    pidList: [],
    roleList: [],
    userListDataCount: null,
    pageSize: 20,
    userListHistoryData: [],
    row: {
      uname: '',
      password: '',
      tel: '',
      ctime: '',
      pid: '',
      img: '',
      state: '',
      role: ''
    }
  },
  getters: {
    userList: state => state.userList
  },
  mutations: {
    [SYSTEM_USER_LIST] (state, data) {
      state.userList = data
    },
    [USERLIST_CURRENT_ROW] (state, data) {
      state.row = data
    },
    [USERLIST_DATA_COUNT] (state, data) {
      state.userListDataCount = data
    },
    [USERLIST_HISTORY_DATA] (state, data) {
      state.userListHistoryData = data
    },
    [ROLE_LIST] (state, data) {
      state.roleList = data
    },
    [PID_LIST] (state, data) {
      state.pidList = data
    },
    [SYSTEM_USER_UPLOAD_LIST] (state, data) {
      state.userList = state.userList.push(data)
    }
  },
  actions: {
    async getSystemUserListTable ({ commit, rootState }) {
      let data = {}
      let newData = []
      const resp = await getUserListTable()
      resp.data.data.map((item) => {
        newData = [...newData, {
          id: item.id,
          uname: item.uname,
          password: item.password,
          pname: item.pname ? item.pname : '-',
          tel: item.tel,
          ctime: item.ctime,
          state: item.state.toString(),
          role: item.role.toString(),
          gname: item.gname,
          pid: item.pid,
          img: item.img
        }]
      })
      commit(SYSTEM_USER_LIST, newData)
      commit(USERLIST_DATA_COUNT, newData.length)
      if (newData.length < rootState.system.pageSize) {
        commit(USERLIST_HISTORY_DATA, newData)
      } else {
        commit(USERLIST_HISTORY_DATA, newData.slice(0, rootState.system.pageSize))
      }
    },
    async changeUserListPage ({ commit, rootState }, index) {
      let data = rootState.system.userList
      let _start = (index - 1) * rootState.system.pageSize
      let _end = index * rootState.system.pageSize
      commit(USERLIST_HISTORY_DATA, data.slice(_start, _end))
    },
    async editUserlistData ({ commit, rootState }, playload) {
      let data = {}
      if (Object.keys(playload).length > 0) {
        data = playload
      }
      await editUserlistRecord(data)
    },
    async getUserlistCurrentRow ({ commit, rootState }, row) {
      commit(USERLIST_CURRENT_ROW, row)
    },
    async getRoleList ({ commit, rootState }) {
      const resp = await getRole({})
      let newData = []
      resp.data.data.map((item) => {
        newData = [{
          role: item.role,
          gname: item.gname
        }, ...newData]
      })
      commit(ROLE_LIST, newData)
    },
    async getPidList ({ commit, rootState }) {
      const resp = await getPid({})
      let newData = []
      resp.data.data.map((item) => {
        newData = [...newData, {
          id: item.id,
          uname: item.uname
        }]
      })
      commit(PID_LIST, newData)
    },
    async getSystemUserUploadList ({ commit, rootState }, uploadList) {
      commit(SYSTEM_USER_UPLOAD_LIST, uploadList)
    }
  }
}
