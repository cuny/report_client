import {
  getTurnOverTable
} from '../../api/employees'
import {
  TURN_OVER_LIST,
  TURN_OVER_CURRENT_ROW,
  TURN_OVER_DATA_COUNT,
  TURN_OVER_HISTORY_DATA,
  TURN_OVER_UPLOAD_LIST
} from '../motations-type'

export default {
  state: {
    turnOverList: [],
    turnOverDataCount: null,
    pageSize: 20,
    turnOverHistoryData: [],
    row: {
      name: '',
      innum: '',
      leave: '',
      allnum: '',
      rate: ''
    }
  },
  getters: {
    turnOverList: state => state.turnOverList
  },
  mutations: {
    [TURN_OVER_LIST] (state, data) {
      state.turnOverList = data
    },
    [TURN_OVER_CURRENT_ROW] (state, data) {
      state.row = data
    },
    [TURN_OVER_DATA_COUNT] (state, data) {
      state.turnOverDataCount = data
    },
    [TURN_OVER_HISTORY_DATA] (state, data) {
      state.turnOverHistoryData = data
    },
    [TURN_OVER_UPLOAD_LIST] (state, data) {
      state.turnOverList = state.turnOverList.push(data)
    }
  },
  actions: {
    async getTurnOverList ({ commit, rootState }, playload) {
      let data = {}
      let newData = []
      if (Object.keys(playload).length > 0) {
        data = playload
      }
      const resp = await getTurnOverTable(data)
      resp.data.data.map((item) => {
        newData = [...newData, {
          id: item.id,
          name: item.name ? item.name : '-',
          innum: item.innum,
          leave: item.leave,
          allnum: item.allnum,
          rate: item.rate
        }]
      })
      commit(TURN_OVER_LIST, newData)
      commit(TURN_OVER_DATA_COUNT, newData.length)
      if (newData.length < rootState.turnOver.pageSize) {
        commit(TURN_OVER_HISTORY_DATA, newData)
      } else {
        commit(TURN_OVER_HISTORY_DATA, newData.slice(0, rootState.turnOver.pageSize))
      }
    },
    // async updateSaleRecordData ({ commit, rootState }, playload) {
    //   let data = {}
    //   if (Object.keys(playload).length > 0) {
    //     data = playload
    //   }
    //   await editeSaleRecord(data)
    // },
    async getTurnOverCurrentRow ({ commit, rootState }, row) {
      commit(TURN_OVER_CURRENT_ROW, row)
    },
    async changeTurnOverPage ({ commit, rootState }, index) {
      let data = rootState.turnOver.turnOverList
      let _start = (index - 1) * rootState.turnOver.pageSize
      let _end = index * rootState.turnOver.pageSize
      commit(TURN_OVER_HISTORY_DATA, data.slice(_start, _end))
    },
    async getTurnOverUploadList ({ commit, rootState }, uploadList) {
      commit(TURN_OVER_UPLOAD_LIST, uploadList)
    }
  }
}
