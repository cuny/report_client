import {
  getReportTable
} from '../../api/employees'
import {
  REPORT_LIST,
  REPORT_CURRENT_ROW,
  REPORT_DATA_COUNT,
  REPORT_HISTORY_DATA,
  REPORT_UPLOAD_LIST
} from '../motations-type'

export default {
  state: {
    reportList: [],
    reportDataCount: null,
    pageSize: 20,
    reportHistoryData: [],
    row: {
      name: '',
      invite: '',
      order: '',
      view: '',
      entry: ''
    }
  },
  getters: {
    reportList: state => state.reportList
  },
  mutations: {
    [REPORT_LIST] (state, data) {
      state.reportList = data
    },
    [REPORT_CURRENT_ROW] (state, data) {
      state.row = data
    },
    [REPORT_DATA_COUNT] (state, data) {
      state.reportDataCount = data
    },
    [REPORT_HISTORY_DATA] (state, data) {
      state.reportHistoryData = data
    },
    [REPORT_UPLOAD_LIST] (state, data) {
      state.reportList = state.reportList.push(data)
    }
  },
  actions: {
    async getReportList ({ commit, rootState }, playload) {
      let data = {}
      let newData = []
      if (Object.keys(playload).length > 0) {
        data = playload
      }
      const resp = await getReportTable(data)
      resp.data.data.map((item) => {
        newData = [...newData, {
          id: item.id,
          mname: item.mname ? item.mname : '-',
          invitenum: item.invitenum,
          ordernum: item.ordernum,
          viewnum: item.viewnum,
          entrynum: item.entrynum
        }]
      })
      commit(REPORT_LIST, newData)
      commit(REPORT_DATA_COUNT, newData.length)
      if (newData.length < rootState.report.pageSize) {
        commit(REPORT_HISTORY_DATA, newData)
      } else {
        commit(REPORT_HISTORY_DATA, newData.slice(0, rootState.report.pageSize))
      }
    },
    // async updateSaleRecordData ({ commit, rootState }, playload) {
    //   let data = {}
    //   if (Object.keys(playload).length > 0) {
    //     data = playload
    //   }
    //   await editeSaleRecord(data)
    // },
    async getReportCurrentRow ({ commit, rootState }, row) {
      commit(REPORT_CURRENT_ROW, row)
    },
    async changeReportPage ({ commit, rootState }, index) {
      let data = rootState.report.reportList
      let _start = (index - 1) * rootState.report.pageSize
      let _end = index * rootState.report.pageSize
      commit(REPORT_HISTORY_DATA, data.slice(_start, _end))
    },
    async getReportUploadList ({ commit, rootState }, uploadList) {
      commit(REPORT_UPLOAD_LIST, uploadList)
    }
  }
}
