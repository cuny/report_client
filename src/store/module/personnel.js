import {
  getPersonnelTable,
  editePersonnelRecord
} from '../../api/employees'
import {
  PERSONNEL_LIST,
  PERSONNEL_CURRENT_ROW,
  PERSONNEL_DATA_COUNT,
  PERSONNEL_HISTORY_DATA,
  PERSONNEL_UPLOAD_LIST
} from '../motations-type'

export default {
  state: {
    personnelList: [],
    personnelDataCount: null,
    pageSize: 20,
    personnelHistoryData: [],
    row: {
      name: '',
      tel: '',
      job: '',
      viewtime: '',
      viewhour: '',
      viewresult: '',
      isreview: '',
      reviewtime: '',
      reviewresult: '',
      entrytime: '',
      entrydepart: '',
      entryaddress: '',
      pcode: '',
      state: '',
      uptime: '',
      ctime: ''
    }
  },
  getters: {
    personnelList: state => state.personnelList
  },
  mutations: {
    [PERSONNEL_LIST] (state, data) {
      state.personnelList = data
    },
    [PERSONNEL_CURRENT_ROW] (state, data) {
      state.row = data
    },
    [PERSONNEL_DATA_COUNT] (state, data) {
      state.personnelDataCount = data
    },
    [PERSONNEL_HISTORY_DATA] (state, data) {
      state.personnelHistoryData = data
    },
    [PERSONNEL_UPLOAD_LIST] (state, data) {
      state.personnelList = state.personnelList.push(data)
    }
  },
  actions: {
    async getPersonnelList ({ commit, rootState }, playload) {
      let data = {}
      let newData = []
      let newTotal = null
      if (Object.keys(playload).length > 0) {
        data = playload
      }
      const resp = await getPersonnelTable(data)
      resp.data.data.rows.map((item) => {
        newData = [...newData, {
          id: item.id,
          muid: item.muid.toString(),
          name: item.name,
          tel: item.tel,
          job: item.job,
          viewtime: item.viewtime,
          viewhour: item.viewhour,
          viewresult: item.viewresult,
          isreview: item.isreview.toString(),
          reviewtime: item.reviewtime,
          reviewresult: item.reviewresult ? item.reviewresult : '-',
          entrytime: item.entrytime,
          entrydepart: item.entrydepart ? item.entrydepart : '-',
          entryaddress: item.entryaddress ? item.entryaddress : '-',
          pcode: item.pcode ? item.pcode : '-',
          state: item.state.toString(),
          uptime: item.uptime,
          ctime: item.ctime
        }]
      })
      commit(PERSONNEL_LIST, newData)
      commit(PERSONNEL_DATA_COUNT, resp.data.data.total)
      if (newData.length < rootState.personnel.pageSize) {
        commit(PERSONNEL_HISTORY_DATA, newData)
      } else {
        commit(PERSONNEL_HISTORY_DATA, newData.slice(0, rootState.personnel.pageSize))
      }
    },
    async changePersonnelPage ({ commit, rootState }, index) {
      let data = rootState.personnel.personnelList
      let _start = (index - 1) * rootState.personnel.pageSize
      let _end = index * rootState.personnel.pageSize
      commit(PERSONNEL_HISTORY_DATA, data.slice(_start, _end))
    },
    async updatePersonnelData ({ commit, rootState }, playload) {
      let data = {}
      if (Object.keys(playload).length > 0) {
        data = playload
      }
      await editePersonnelRecord(data)
    },
    async getPersonnelCurrentRow ({ commit, rootState }, row) {
      commit(PERSONNEL_CURRENT_ROW, row)
    },
    async getPersonnelUploadList ({ commit, rootState }, uploadList) {
      commit(PERSONNEL_UPLOAD_LIST, uploadList)
    }
  }
}
