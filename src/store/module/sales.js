import {
  getSaleTable,
  editeSaleRecord
} from '../../api/sale'
import {
  SALE_LIST,
  SALE_UPLOAD_LIST,
  CURRENT_ROW,
  SALE_DATA_COUNT,
  SALE_HISTORY_DATA
} from '../motations-type'

export default {
  state: {
    saleList: [],
    dataCount: null,
    pageSize: 20,
    historyData: [],
    row: {
      name: '',
      uname: '',
      pname: '',
      ckname: '',
      tel: '',
      wechat: '',
      ctime: '',
      checkuid: '',
      comedate: '',
      ymoney: '',
      money: '',
      orderdate: '',
      type: '',
      state: '',
      des: ''
    }
  },
  getters: {
    saleList: state => state.saleList
  },
  mutations: {
    [SALE_LIST] (state, data) {
      state.saleList = data
    },
    [CURRENT_ROW] (state, data) {
      state.row = data
    },
    [SALE_DATA_COUNT] (state, data) {
      state.dataCount = data
    },
    [SALE_HISTORY_DATA] (state, data) {
      state.historyData = data
    },
    [SALE_UPLOAD_LIST] (state, data) {
      state.saleList = state.saleList.push(data)
    }
  },
  actions: {
    async getSaleList ({ commit, rootState }, playload) {
      let data = {}
      let newData = []
      if (Object.keys(playload).length > 0) {
        data = playload
      }
      const resp = await getSaleTable(data)
      resp.data.data.rows.map((item) => {
        newData = [...newData, {
          id: item.id,
          name: item.name ? item.name : '-',
          uname: item.uname ? item.uname : '-',
          pname: item.pname ? item.pname : '-',
          ckname: item.ckname ? item.ckname : '-',
          ymoney: item.ymoney,
          money: item.money,
          tel: item.tel ? item.tel.toString() : '-',
          wechat: item.wechat ? item.wechat : '-',
          ctime: item.ctime,
          orderdate: item.orderdate,
          comedate: item.comedate,
          type: item.type ? item.type : '-',
          state: item.state.toString(),
          checkuid: item.checkuid ? item.checkuid : '-',
          puid: item.puid,
          muid: item.muid,
          des: item.des ? item.des : '-',
          cz: item.cz
        }]
      })
      commit(SALE_LIST, newData)
      commit(SALE_DATA_COUNT, resp.data.data.total)
      if (newData.length < rootState.sales.pageSize) {
        commit(SALE_HISTORY_DATA, newData)
      } else {
        commit(SALE_HISTORY_DATA, newData.slice(0, rootState.sales.pageSize))
      }
    },
    async updateSaleData ({ commit, rootState }, playload) {
      let data = {}
      if (Object.keys(playload).length > 0) {
        data = playload
      }
      await editeSaleRecord(data)
    },
    async getCurrentRow ({ commit, rootState }, row) {
      commit(CURRENT_ROW, row)
    },
    async changeSalePage ({ commit, rootState }, index) {
      let data = rootState.sales.saleList
      let _start = (index - 1) * rootState.sales.pageSize
      let _end = index * rootState.sales.pageSize
      commit(SALE_HISTORY_DATA, data.slice(_start, _end))
    },
    async getSaleUploadList ({ commit, rootState }, uploadList) {
      commit(SALE_UPLOAD_LIST, uploadList)
    }
  }
}
