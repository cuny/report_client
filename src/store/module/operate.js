import {
  getOperateTable,
  editOperateRecord
} from '../../api/customer'
import {
  OPERATE_LIST,
  OPERATE_CURRENT_ROW,
  PROID_LIST,
  OPERATE_DATA_COUNT,
  OPERATE_HISTORY_DATA,
  PROJECT,
  OPERATE_UPLOAD_LIST
} from '../motations-type'

export default {
  state: {
    operateList: [],
    project: [],
    operateDataCount: null,
    pageSize: 20,
    operateHistoryData: [],
    row: {
      id: '',
      name: '',
      ctime: '',
      unitprice: '',
      state: '',
      url: ''
    },
    proid: []
  },
  getters: {
    operateList: state => state.operateList
  },
  mutations: {
    [OPERATE_LIST] (state, data) {
      state.operateList = data
    },
    [OPERATE_CURRENT_ROW] (state, data) {
      state.row = data
    },
    [PROID_LIST] (state, data) {
      state.row = data
    },
    [OPERATE_DATA_COUNT] (state, data) {
      state.operateDataCount = data
    },
    [OPERATE_HISTORY_DATA] (state, data) {
      state.operateHistoryData = data
    },
    [PROJECT] (state, data) {
      state.project = data
    },
    [OPERATE_UPLOAD_LIST] (state, data) {
      state.operateList = state.operateList.push(data)
    }
  },
  actions: {
    async getOperateList ({ commit, rootState }, playload) {
      let data = {}
      let newData = []
      if (Object.keys(playload).length > 0) {
        data = playload
      }
      const resp = await getOperateTable(data)
      resp.data.data.rows.map((item) => {
        newData = [...newData, {
          id: item.id,
          proid: item.proid,
          name: item.name,
          ctime: item.ctime,
          unitprice: item.unitprice,
          state: item.state.toString(),
          url: item.url ? item.url : '-'
        }]
      })
      commit(OPERATE_LIST, newData)
      commit(OPERATE_DATA_COUNT, resp.data.data.total)
      if (newData.length < rootState.operate.pageSize) {
        commit(OPERATE_HISTORY_DATA, newData)
      } else {
        commit(OPERATE_HISTORY_DATA, newData.slice(0, rootState.operate.pageSize))
      }
    },
    async getProjects ({ commit, rootState }) {
      const resp = await getOperateTable({})
      let newData = []
      resp.data.data.rows.map((item) => {
        newData = [{
          id: item.id,
          name: item.name
        }, ...newData]
      })
      commit(PROJECT, newData)
    },
    async changeOperatePage ({ commit, rootState }, index) {
      let data = rootState.operate.operateList
      let _start = (index - 1) * rootState.operate.pageSize
      let _end = index * rootState.operate.pageSize
      commit(OPERATE_HISTORY_DATA, data.slice(_start, _end))
    },
    async getOperateCurrentRow ({ commit, rootState }, row) {
      commit(OPERATE_CURRENT_ROW, row)
    },
    async editOperateData ({ commit, rootState }, playload) {
      let data = {}
      if (Object.keys(playload).length > 0) {
        data = playload
      }
      await editOperateRecord(data)
    },
    async getProidList ({ commit, rootState }) {
      let proidList = []
      const resp = await getOperateTable({})
      resp.data.data.rows.map((item) => {
        proidList.push(item.id)
      })
      commit(PROID_LIST, proidList)
    },
    async getOperateUploadList ({ commit, rootState }, uploadList) {
      commit(OPERATE_UPLOAD_LIST, uploadList)
    }
  }
}
