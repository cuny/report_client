import Vue from 'vue'
import Vuex from 'vuex'

import user from './module/user'
import app from './module/app'
import sales from './module/sales'
import business from './module/business'
import personnel from './module/personnel'
import employee from './module/employee'
import system from './module/system'
import customer from './module/customer'
import statistical from './module/statistical'
import operate from './module/operate'
import modal from './module/modal'
import department from './module/department'
import saleRecord from './module/saleRecord'
import report from './module/report'
import turnOver from './module/turnOver'
import saleTarget from './module/saleTarget'
import rank from './module/rank'
import home from './module/home'
import perCost from './module/perCost'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    //
  },
  mutations: {
    //
  },
  actions: {
    //
  },
  modules: {
    user,
    app,
    sales,
    business,
    personnel,
    employee,
    system,
    customer,
    statistical,
    operate,
    modal,
    department,
    saleRecord,
    report,
    turnOver,
    saleTarget,
    rank,
    home,
    perCost
  }
})
